import requests, json
from pprint import pprint as pp

def ping():
    
    url = 'http://127.0.0.1:8000/test'
    
    item = {"request":"ping"}
    
    results = requests.post(url=url, json = item)
  
    pp(results.json())
    print ("__________")

    print (results.status_code)
    print ("__________")
    
    print(results.headers)
    print ("__________")
    
    
if __name__ == "__main__":
    ping()