# Flask Gunicorn Boilerplate
This is a simple boiler code that sets up gunicord + flask environment

as an example run gunicorn locally or as service at any localhost:any_port, one worker

> gunicorn -b localhost:8000 wsgi --reload

then run

> python tests/test.py for ping pong request

all other code will be handled from within this boilerplate and devops just needs to forward requests to either localhost or socket