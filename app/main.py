from flask import Flask, request


app = Flask(__name__)


@app.route('/test', methods=['GET','POST'])
def pingpong():
    if request.json['request'] == 'ping':
        return {'response':'pong'}
    else:
        return {'response':'Error'}